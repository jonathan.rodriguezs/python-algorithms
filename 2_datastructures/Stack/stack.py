# Implement a stack
stack = []

# push items to the stack
stack.append(1)
stack.append(2)
stack.append(3)
stack.append(4)

# print the stack
print(stack)

# pop an item from the stack
x = stack.pop() # [1, 2, 3], returns 4
print("Pooped element: ", x)

print(stack)
