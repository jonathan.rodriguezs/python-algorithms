from collections import deque

queue = deque()

# add items to the queue
queue.append(1)
queue.append(2)
queue.append(3)
queue.append(4)

# print the queue
print(queue)

# remove elements
x = queue.popleft()
print("Dequeued element: ", x)

print(queue)
