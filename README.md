# Python algorithms

> Brief introduction to the most common algorithms in CS

## Topics
- Data structures foundations
- Recursion
- Search algorithms
- Sorting algorithms

***

## Author
💻 Jonathan Rodriguez